# NEST JS TODODEV TEMPLATE
Author : Samuel LITZLER

There are tags for different types of starting projets.

NestJS is used with typescript.

There are controllers / modules / service / providers / Pipes / Guards/ Interceptors and your can create custom decorators.

# Dependencies
npm i -g @nestjs/cli
nest new template-nestjs --strict
-> yarn
-> 


## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```